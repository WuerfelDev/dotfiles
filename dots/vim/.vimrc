set relativenumber
set number

set linebreak
set showbreak=+++
set scrolloff=5

set virtualedit=block
set noerrorbells

set showmatch " Shows matching brackets, jump with %

set hlsearch " Highlight all matches, :nohl to unhighlight
set ignorecase
set smartcase " Search case-insenstitive if all lowercase
set incsearch " Instant highlight while searching

set autoindent
set expandtab
set shiftwidth=4
set smartindent
set smarttab
set softtabstop=4


set ruler

set undolevels=1000
set backspace=indent,eol,start


set nostartofline
filetype indent plugin on
syntax on
set wildmenu

" Use :w!! to save file using root privileges
cmap w!! w !sudo tee > /dev/null %
