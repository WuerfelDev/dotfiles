set -gx EDITOR hx
set -gx VISUAL hx
set -gx TEALDEER_CONFIG_DIR ~/.config/tealdeer/ #use same config dir on mac

set -g fish_key_bindings fish_vi_key_bindings

fish_add_path --path /opt/homebrew/bin/
if pgrep -x "Zscaler" &> /dev/null
    # echo "[Zscaler running - setting proxy env]"
    set -gx http_proxy http://127.0.0.1:9000
    set -gx https_proxy http://127.0.0.1:9000
end


function fish_greeting
    set -l greeting_text "Hi!"
    if type -q cowsay
        and test $COLUMNS -gt 80

        #cowsay -f (ls /usr/share/cowsay/cows | shuf -n1) "Hi!" #| lolcat -S 60
        cowsay -f stegosaurus.cow "$greeting_text"
    else
        echo "$greeting_text"
    end
end


# Quick Bash
function qb
    echo -e "\e[7m ---- Running as bash ---- \e[27m"
    if count $argv &> /dev/null
        bash -c "$argv"
    else
        # You can use this to avoid wildcard problems
        bash -t
    end
end

# mkdir + cd
function mkcd
    mkdir -p "$argv" && cd "$argv"
end

function raop
    # disable -> unload raop module
    if test (count $argv) -eq 1 -a "$argv[1]" = "disable"
        pactl unload-module (pactl list short modules | grep 'module-raop-discover' | awk '{print $1}')
        return 0
    end

    if test (count $argv) -eq 1 -a "$argv[1]" = "fix"
        systemctl --user restart wireplumber pipewire pipewire-pulse
        return 0
    end

    # Start raop module if not started yet
    if not pactl list short modules | grep 'module-raop-discover' &> /dev/null
        pactl load-module module-raop-discover
        sleep 2
    end
    set -l searchfor "$argv[1]" # Optional grep for advanced sink selection
    if not pactl set-default-sink (pactl list short sinks | grep 'raop_sink' | grep "$searchfor" | awk '{print $1}')
        echo "Problem selecting a unique sink. Available sinks:"
        pactl list short sinks | grep 'raop_sink' | awk '{print "> " substr($2,11)}'
    end
end

# dark background for simple commands
# usage: dark; echo "mycommand"
function dark
    set_color -b black
end

# https://fishshell.com/docs/current/relnotes.html#fish-3-6-0-released-january-7-2023
function last_history_item; echo $history[1]; end
abbr -a !! --position anywhere --function last_history_item

# https://fishshell.com/docs/current/relnotes.html#fish-3-6-0-released-january-7-2023
function multicd
    echo cd (string repeat -n (math (string length -- $argv[1]) - 1) ../)
end
abbr --add dotdot --regex '^\.\.+$' --function multicd


abbr q ' exit'
abbr :q ' exit'
abbr sl ' sl -e'
abbr - 'cd ..'

abbr vim 'nvim'
abbr vi 'nvim'
abbr nvmi 'nvim'
abbr view 'nvim -R'
abbr vimdiff 'nvim -d'
abbr realvi 'vi'
abbr realvim 'vim'


abbr hg 'history | grep -i'
abbr gg 'git grep -in'
abbr xo 'xdg-open'

abbr brave 'brave -enable-features=UseOzonePlatform -ozone-platform=wayland' #Wayland fix
abbr android-studio '_JAVA_AWT_WM_NONREPARENTING=1 android-studio' #Wayland fix
abbr cura 'cura -platformtheme gtk3' #https://github.com/Ultimaker/Cura/issues/12266


# Start yaft as tty terminal terminal 
# Local login
if status is-login
    and test -z "$DISPLAY"
    and test -z "$TMUX"
    and test "$TERM" = "linux"
    and not string match -qr '((sshd?)|mosh)' (who am i)
    and test -z "$SSH_CLIENT"
    and test -z "$SSH_CONNECTION"
    and test -z "$SSH_TTY"

    if string match -q "/dev/tty1" (tty)
        and type -q sway
        sway
    else if type -q yaft
        yaft
    else if type -q tmux
        tmux new-session
    end


# Start tmux if not yet started
else if type -q tmux
    #and test -z "$DISPLAY"
    and test -z "$TMUX"
    and test "$TERM" != "xterm-256color" #fix to launch alacritty on mac / this fails on yaft because we set term to this exact string in the custom config

    tmux new-session
end


# Keep at the bottom
if type -q starship
    starship init fish | source
end
