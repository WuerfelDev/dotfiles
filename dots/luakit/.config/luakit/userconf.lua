local settings = require "settings"
local newtab_chrome = require "newtab_chrome"

-- SETTINGS

settings.application.prefer_dark_mode = true

settings.session.always_save = true

settings.webview.user_agent = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:54.0) Gecko/20100101 Firefox/54.0"

settings.webview.enable_smooth_scrolling = true
settings.window.scroll_step = 40

-- to configure newtab page see: https://github.com/luakit/luakit/issues/694
settings.window.home_page = "https://search.brave.com"
settings.window.search_engines.brave = "https://search.brave.com/search?q=%s"
settings.window.search_engines.aur = "https://aur.archlinux.org/packages/?O=0&K=%s"
settings.window.default_search_engine = "brave"


--- SEARCH ENGINES
---local engines = settings.window.search_engines
---engines.aur     = "https://aur.archlinux.org/packages.php?O=0&K=%s&do_Search=Go"
---engines.brave   = "https://brave.com/search?q=%s"

---engines.default = search_engines.brave

--- HANDLE MAILTO LINKS
--- might add later. Resources:
--- - https://luakit.github.io/docs/pages/02-faq.html
--- - https://github.com/RainLoop/rainloop-webmail/issues/1856


-- MODULES

-- Set dvorak chars as labels
local select = require "select"
select.label_maker = function ()
    local chars = interleave("aoeui", "dhtns")
    return trim(sort(reverse(chars)))
end

-- hide non-matching labels
local follow = require "follow"
follow.pattern_maker = follow.pattern_styles.match_label
