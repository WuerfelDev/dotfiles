#!/usr/bin/env python3
import stat,os
import threading
import time
import sys
from enum import Enum

UpdateState = Enum('UpdateStates','NONE TEXT SURROUND')
DirectionState = Enum('DirectionState','AUTO ALTERNATE FORWARD TRUNCATE ELLIPSIS')


class ScrollText:
    
    def __init__(self, length=30, direction=DirectionState.AUTO):
        self.length = length
        self.direction = direction
        
        self.text = ""
        self.pre = ""
        self.post = ""
        self.escape = []

        self.speed = .3
        self.update = UpdateState.NONE # restarts the run loop

        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True
        thread.start()

    # escaping characters, rep is an array of [find, replace],
    def setEscape(self,rep):
        self.escape = rep

    def setText(self,text):
        text = text.strip()
        if text != self.text:
            self.text = text
            self.update = UpdateState.TEXT
    
    def setPre(self,text):
        if text != self.pre:
            self.pre = text
            self.update = UpdateState.SURROUND
        
    def setPost(self,text):
        if text != self.post:
            self.post = text
            self.update = UpdateState.SURROUND

    def wait_and_restart(self):
        time.sleep(self.speed)
        return self.update == UpdateState.TEXT

    def wait_for_update(self):
        while self.update == UpdateState.NONE:
            time.sleep(self.speed)

    def print(self,text):
        # Escape characters
        for x in self.escape:
            text = text.replace(x[0],x[1])
        # add pre and post
        text = self.pre + text + self.post
        if sys.stdout and sys.stdout.isatty():
            # Print in-line in terminal
            print(text,end='\r')
        else:
            # Flush for waybar
            print(text,flush=True)

    def run(self):
        while True:
            self.update = UpdateState.NONE
            
            direction = self.direction # modify direction only temporary
            # Decide direction for AUTO
            if direction == DirectionState.AUTO:
                if len(self.text) < self.length + 2:
                    direction = DirectionState.ELLIPSIS
                elif len(self.text) < self.length*1.5:
                    direction = DirectionState.ALTERNATE
                else:
                    direction = DirectionState.FORWARD
            
            # TRUNCATE + catch too short
            if direction == DirectionState.TRUNCATE or len(self.text) <= self.length:
                self.print(self.text[:self.length].rstrip())
                self.wait_for_update()
            # ELLIPSIS
            elif direction == DirectionState.ELLIPSIS:
                self.print(self.text[:self.length - 1].rstrip() + "…")
                self.wait_for_update()
            # ALTERNATE
            elif direction == DirectionState.ALTERNATE:
                self.run_alternate()
            # FORWARD
            elif direction == DirectionState.FORWARD:
                self.run_forward()


    def run_alternate(self):
        text_len = len(self.text)
        for x in range(text_len - self.length + 1):
            self.print(self.text[x:x+self.length])
            if self.wait_and_restart():
                return
        for x in range(text_len - self.length + 1):
            self.print(self.text[text_len-x-self.length:text_len-x])
            if self.wait_and_restart():
                return

    def run_forward(self):
        text = self.text + " "*6
        text_len = len(text)
        text = text+text
        for x in range(text_len):
            self.print(text[x:x+self.length])
            if self.wait_and_restart():
                return
