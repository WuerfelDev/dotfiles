#!/usr/bin/env python3
#sudo apt install python3-gi python3-gi-cairo gir1.2-gtk-3.0 gir1.2-playerctl-2.0

# Resources:
# https://lazka.github.io/pgi-docs/#Playerctl-2.0
# https://github.com/altdesktop/playerctl/blob/master/examples/notify.py

# Advancement for future: use json; something like this
# {\"text\": \"{{title}}\", \"tooltip\": \"{{playerName}} : {{title}}\", \"alt\": \"{{status}}\", \"class\": \"{{status}}\"}

import gi
gi.require_version('Playerctl', '2.0')
from gi.repository import Playerctl, GLib
import re
import ScrollText


def format_title(title):
    # Remove '(official music video) - YouTube' or similar from title
    return re.sub(r'(\s?[\(|\[](official\s?)?(music\s)?(lyrics?\s?)?(video)?(audio)?[\)|\]])?(\s-\sYouTube)?', '', title, flags=re.IGNORECASE)

def on_status(player,status):
    symbols = ['♪','',''] # Playing, Paused, Stopped
    scroll.setPre(symbols[status])

def on_metadata(player, metadata):
    keys = metadata.keys()
    if 'xesam:title' in keys:
        scroll.setText(format_title(metadata['xesam:title']))


def on_name_appeared(manager, name):
    player = Playerctl.Player.new_from_name(name)
    player.connect('metadata', on_metadata)
    player.connect('playback-status', on_status)
    manager.manage_player(player)
    # update output with data from current player
    on_metadata(player,player.props.metadata)
    on_status(player,player.props.playback_status)



scroll = ScrollText.ScrollText(40)
scroll.setEscape([["&","&amp;"]])

manager = Playerctl.PlayerManager()
manager.connect('name-appeared', on_name_appeared)

# Init existing players
for name in manager.props.player_names:
    on_name_appeared(manager, name)

try:
    GLib.MainLoop().run()
except KeyboardInterrupt:
    GLib.MainLoop().quit()
