#!/usr/bin/env bash

padding=10
gaps=20

if [[ $(yabai -m query --windows --space | jq '. | map(select(.["is-visible"])) | map(select(.["is-floating"] == false)) | length') == 1 ]]; then
    #TODO: if possible remove border (but keep blur)
    yabai -m space --gap abs:0
    yabai -m space --padding abs:0:0:0:0
else
    # reset to defaults
    yabai -m space --gap abs:$gaps
    yabai -m space --padding abs:$padding:$padding:$padding:$padding
fi
