local builtin = require('telescope.builtin')
vim.keymap.set('n', '<leader>fd', builtin.find_files, {}) -- find in dir
vim.keymap.set('n', '<leader>fg', builtin.git_files, {}) -- find in git
vim.keymap.set('n', '<leader>gs', function()
	builtin.grep_string({ search = vim.fn.input("Grep > ") })
end)
