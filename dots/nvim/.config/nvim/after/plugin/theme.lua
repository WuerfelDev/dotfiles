vim.o.background = "dark"

local function transparentEnable()
  require("gruvbox").setup({
    transparent_mode = true,
    overrides = {
        NormalFloat = {
            -- No transparency on floating windows like Mason
            bg = require("gruvbox").palette["dark1"],
            fg = require("gruvbox").palette["light1"],
          }
    },
  })
  vim.cmd.colorscheme('gruvbox')
end

local function transparentDisable()
  require("gruvbox").setup({
    transparent_mode = false,
  })
  vim.cmd.colorscheme('gruvbox')
end


-- Enable by default
transparentEnable()

vim.api.nvim_create_user_command('TransparentDisable', transparentDisable, {})
vim.api.nvim_create_user_command('TransparentEnable', transparentEnable, {})
