-- Install packer first
-- https://github.com/wbthomason/packer.nvim#quickstart


-- Only required if you have packer configured as `opt`
vim.cmd.packadd('packer.nvim')

return require('packer').startup(function(use)
  -- Packer can manage itself
  use('wbthomason/packer.nvim')

  -- File format tweaks
  use {
    'RaafatTurki/hex.nvim',
    config = function ()
      require('hex').setup()
    end
  }

  use {
    'NvChad/nvim-colorizer.lua',
    config = function ()
      require("colorizer").setup({
        filetypes = {
          '*',
          css = { rgb_fn = true },
          html = { rgb_fn = true },
          conf = { AARRGGBB = true },
          "!mason",
          "!netrw",
          "!vim",
          "!help",
        },
      })
    end
  }


  -- :SudaWrite for writing files as sudo
  use('lambdalisue/suda.vim')

  use('mbbill/undotree')

  -- Live preview for eg :35
  use {
    'nacro90/numb.nvim',
    config = function ()
      require('numb').setup({centered_peeking=false})
    end
  }

  -- Comments
  use {
    'terrortylor/nvim-comment',
    config = function ()
      require('nvim_comment').setup({comment_empty = false})
    end
  }

  -- automatically detect and adjust tab width in files
  use('tpope/vim-sleuth')

  -- Git
  use('tpope/vim-fugitive')
  use('shumphrey/fugitive-gitlab.vim')
  use('tpope/vim-rhubarb')
  use('tommcdo/vim-fubitive')
  use {
    'lewis6991/gitsigns.nvim',
    config = function()
      require('gitsigns').setup {current_line_blame = true}
    end
  }

  -- Theming
  use('ellisonleao/gruvbox.nvim')


  use { 'nvim-treesitter/nvim-treesitter', run = ':TSUpdate' }
  use {
    'lewis6991/foldsigns.nvim',
    config = function()
      require('foldsigns').setup {
        exclude = {'GitSigns.*'},
      }
    end
  }

  -- Telescope
  -- Requires a fuzzy finder (eg ripgrep)
  use {
    'nvim-telescope/telescope.nvim', tag = '0.1.1',
    config = function()
      require('telescope').setup({
        defaults = {
          file_ignore_patterns = { "./node_modules/*", "node_modules", "build/*" },
          initial_mode = "normal",
        }
      });
    end,
    requires = { {'nvim-lua/plenary.nvim'} }
  }

  use {
    'axieax/typo.nvim',
    config = function()
      require('typo').setup({
        ignored_suggestions = { "*.swp", "*.zip", "a.out", "package-lock.json" },
      });
    end
  }

  -- LSP stuff
  use {
    'VonHeikemen/lsp-zero.nvim',
    requires = {
      -- LSP Support
      {'neovim/nvim-lspconfig'},
      {'williamboman/mason.nvim'},
      {'williamboman/mason-lspconfig.nvim'},

      -- Autocompletion
      {'hrsh7th/nvim-cmp'},
      {'hrsh7th/cmp-buffer'},
      {'hrsh7th/cmp-path'},
      {'saadparwaiz1/cmp_luasnip'},
      {'hrsh7th/cmp-nvim-lsp'},
      {'hrsh7th/cmp-nvim-lua'},

      -- Snippets
      {'L3MON4D3/LuaSnip'},
      {'rafamadriz/friendly-snippets'},
    }
  }

  use {
    "https://git.sr.ht/~whynothugo/lsp_lines.nvim",
    config = function()
      require("lsp_lines").setup()
    end,
  }


  -- some fun stuff
  use('tamton-aquib/duck.nvim')
  use('eandrju/cellular-automaton.nvim')

end)
