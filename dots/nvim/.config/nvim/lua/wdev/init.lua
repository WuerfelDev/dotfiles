require("wdev.helper")
require("wdev.set")
require("wdev.remap")
require("wdev.packer")
RequireIfAvailable("wdev.confidential")

local autocmd = vim.api.nvim_create_autocmd

-- Terminal
autocmd("TermOpen", { command = "startinsert"})
vim.keymap.set("t", "<Esc>", "<C-\\><C-n>")
vim.api.nvim_create_user_command('TerminalSendEsc',':call chansend(b:terminal_job_id, "\\<ESC>\\n")' , {})

-- Copy filepath to clipboard
vim.api.nvim_create_user_command("CopyFilepath", 'let @+ = expand("%:p")', {})

-- Applescript comment str fix
autocmd("FileType", {pattern="applescript", callback=function ()
  vim.bo.commentstring = '-- %s'
end})

-- Highlight TODO comments everywhere
autocmd({"WinEnter","VimEnter"}, {callback=function ()
  vim.fn.matchadd('Todo', 'TODO', -1)
end})

-- Highlight whitespaces at end of lines
vim.api.nvim_set_hl(0, "ExtraWhitespace", {link="Error"})
-- autocmd({"WinEnter","VimEnter"}, {command=":silent! call matchadd('ExtraWhitespace', '\\s\\+$', -1)"})
-- autocmd({"WinEnter","VimEnter"}, {callback=function ()
--   vim.fn.matchadd('ExtraWhitespace', '\\s\\+$', -1)
-- end})


-- Set :Shrug command
vim.api.nvim_create_user_command('Shrug', function ()
  vim.api.nvim_put({"¯\\_(ツ)_/¯"}, "b", true, true)
end, {})
