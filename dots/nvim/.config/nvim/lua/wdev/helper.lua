-- https://stackoverflow.com/a/15434737
-- if IsModuleAvailable("wdev.confidential") then require("wdev.confidential") end
local function isModuleAvailable(name)
  if package.loaded[name] then
    return true
  else
    for _, searcher in ipairs(package.searchers or package.loaders) do
      local loader = searcher(name)
      if type(loader) == 'function' then
        package.preload[name] = loader
        return true
      end
    end
    return false
  end
end

function RequireIfAvailable(name)
  if isModuleAvailable(name) then
    require(name)
  end
end
