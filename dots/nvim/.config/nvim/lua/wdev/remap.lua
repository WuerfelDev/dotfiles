vim.g.mapleader = " "

-- leader b instead of :ls
vim.keymap.set('n', '<leader>b', function() require("telescope.builtin").buffers() end, {})

vim.keymap.set("n", "Q", "<nop>")
vim.keymap.set("n", "q:", ":q")

-- duck spawn/ duck kill
vim.keymap.set('n', '<leader>ds', function() require("duck").hatch() end, {})
vim.keymap.set('n', '<leader>dk', function() require("duck").cook() end, {})

-- gravity
vim.keymap.set('n', '<leader>gv', "<cmd>CellularAutomaton make_it_rain<CR>")
