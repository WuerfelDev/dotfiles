-- Side bar
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.scrolloff = 8
vim.opt.signcolumn = "yes" -- for gitgutter

-- Splits
vim.opt.laststatus = 3

-- Disable mouse
vim.opt.mouse = ''

-- Search
vim.opt.hlsearch = true
vim.opt.incsearch = true
vim.opt.ignorecase = true
vim.opt.smartcase = true

-- Undo
vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.undodir = os.getenv("HOME") .. "/.vim/undodir"
vim.opt.undofile = true

-- Tabs
vim.opt.tabstop = 4
-- Disabled in favor of sleuth
-- vim.opt.softtabstop = -4 -- negative -> should use value of shiftwidth
-- vim.opt.shiftwidth = 0 -- zero -> should use value of tabstop
-- vim.opt.expandtab = true
vim.opt.smartindent = true

-- Styling
vim.opt.termguicolors = true --fails on xterm
vim.opt.guicursor = "" -- Cursor is a block
vim.opt.showmatch = true -- Highlight corresponding brakets
vim.opt.showbreak = "+++" -- Lines that are automatically wrapped
vim.opt.cursorline = true


-- netrw
-- vim.g.netrw_browse_split = 1
vim.g.netrw_banner = 0
vim.g.netrw_winsize = 30
vim.g.netrw_preview = 1 -- Vertical split
vim.g.netrw_liststyle = 3 -- Tree style view
vim.g.netrw_keepdir = 0 -- Set browsing directory always as current dir
