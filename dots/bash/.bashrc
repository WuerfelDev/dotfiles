# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Errors on macs default bash installation
# (not in bash 3.2.57 <- apple is scared of GPLv3)
shopt -s cdspell
shopt -s autocd

export EDITOR=nvim
export VISUAL=nvim

alias grep='grep --color=auto'
alias hg='history | grep -i'
alias gg='git grep -in'
alias ls='ls --color=auto'
alias la='ls --color=auto -la'
alias vim='nvim'
alias q=' exit'
alias sl='sl -e'
alias xo='xdg-open'


# keep at the end
eval "$(starship init bash)"
