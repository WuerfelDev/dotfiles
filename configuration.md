# Configuration

Most configuration is already done by the dotfiles


## Enabling Audio

Something like this:
```sh
yay -S pipewire-pulse
systemctl --user enable --now pipewire pipewire-pulse
```

## Hotspot

Bridging AND system Internet usage seems to work only after stopping and starting create_ap.service

```sh
yay -S linux-wifi-hotspot
# create configfile for a brigde to wifi
sudo create_ap -m bridge wlan0 <network adapter> <new ssid> <password> --mkconfig /etc/create_ap.conf
# set as systemservice (start at boot)
systemctl enable --now create_ap
```
URL: https://github.com/lakinduakash/linux-wifi-hotspot
