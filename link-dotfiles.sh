#!/usr/bin/env bash

# Linking dotfiles
# Required package: stow

pushd ~/.dotfiles/dots/ > /dev/null

# Only stow if there are no complications
# Waits until there are no conflicts
until stow -nvt ~ *
do
    echo "Manual action required!"
    read -n 1 -s -r -p "Press any key to try again"
done

stow -t ~ * 
echo -e "\033[32m✓ Dotfiles linked\033[m"

popd > /dev/null
