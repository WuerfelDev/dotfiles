# Dotfiles

![Sway screenshot](screenshot.png)

## Fresh System Installation

### Arch Installation

Follow the files [arch-install.md](./arch-install.md) and [configuration.md](./configuration.md)

<!-- old probably not needed anymore
1. Install arch
2. Link dotfiles
3. [Install yay](https://github.com/Jguer/yay#binary) | add  `yay --save --answerdiff N` as yay/config.json
4. Install all packages lol

Arch package list (Community, Extra, AUR):
```bash
networkmanager wpa_supplicant sudo tmux neovim sway cups grub  stow alacritty fish starship cowsay unzip waybar base-devel wofi bottom man-db

ttf-jetbrains-mono nerd-fonts-jetbrains-mono brave-bin autotiling keepassxc xwayland otf-openmoji mako easyeffects pipewire-pulse plocate grim slurp

fim bat
neofetch

tealdeer libnotify telegram-desktop
jq # waybar scratchpad
playerctl python-gobject # waybar musicinfo script

python-pynvim # nvim binding
```


Todo:
edit something in
/etc/security/faillock.conf to prevent locking for 10min after 3 failed attempts -->

## Linking the dotfiles

**Automatically** using this script `./link-dotfiles.sh`

or **manually**:

```bash
ln -s ~/.dotfiles/dots/nvim/.config/nvim/ ~/.config/nvim
```

## Setting things up

Check out [configuration.md](./configuration.md)

- fish shell as default<br>
`sudo chsh -s $(which fish) $(logname)`

- fim without sudo<br>
`sudo usermod -aG video "$(logname)"`


<!-- old probably not needed anymore

Greetd/tuigreet
`sudo systemctl enable greetd`

edit /etc/greetd/config.toml:
```
command = "tuigreet --cmd sway"
```

Set Brave to work under wayland:
`sudo sed -i 's/^Exec=brave/Exec=brave --ozone-platform=wayland/g' /usr/share/applications/brave-browser.desktop`

Brave as default browser:
`xdg-settings set default-web-browser brave-browser.desktop` -->
