# Arch installation

> Use the official arch installation guide: https://wiki.archlinux.org/title/Installation_guide
>
> My guide makes it just a little more convienient for mysef.

> Last updated in August 2023

## Base System

### Preparation

1. Download the arch linux iso: https://archlinux.org/download/
1. Find device: `watch -dd lsblk` or `fdisk -l`. Unmount all its partitions.
1. Install to stick: `dd bs=4M if=filename.iso of=/dev/<device> status=progress oflag=sync`
1. Boot to BIOS and enable UEFI if available. Check boot order. Save and boot from the device
1. To install it to your boot device set `copytoram` as a kernel flag (<kbd>e</kbd> in Grub, append to the line starting with 'linux'). (use `toram` if it's not an archlinux)

### Installation

1. Set keyboard layout: `loadkeys dvorak`
1. Check UEFI mode: `ls /sys/firmware/efi/efivars`
1. Connect to wifi: `iwctl` -> `station wlan0 connect <SSID>` (You can use <kbd>Tab</kbd> for autocompletion)
1. `timedatectl set-ntp true`
1. Partitioning the installation disk: Find it using `fdisk -l` then run `fdisk /dev/<device>`. Now set the disk up:
    - Short explaination:
        1. <kbd>g</kbd> select gpt table
        1. <kbd>n</kbd> create a new partition
        1. <kbd>t</kbd> select type of the partition
        1. <kbd>w</kbd> write to the disk
    - These are the exact keystrokes you need:<pre>
g <kbd>Enter</kbd>
n <kbd>Enter</kbd> <kbd>Enter</kbd> <kbd>Enter</kbd> +500M <kbd>Enter</kbd>
t <kbd>Enter</kbd> 1 <kbd>Enter</kbd>
n <kbd>Enter</kbd> <kbd>Enter</kbd> <kbd>Enter</kbd> +8G <kbd>Enter</kbd>
t <kbd>Enter</kbd> <kbd>Enter</kbd> 19 <kbd>Enter</kbd>
n <kbd>Enter</kbd> <kbd>Enter</kbd> <kbd>Enter</kbd> <kbd>Enter</kbd>
t <kbd>Enter</kbd> <kbd>Enter</kbd> 23 <kbd>Enter</kbd>
w <kbd>Enter</kbd></pre>
1. Format the partitions: <pre>
mkfs.fat -F 32 /dev/\<device\>1
mkfs.ext4 -L ROOT /dev/\<device\>3
mkswap /dev/\<device\>2</pre>
1. Mount the partitions:<pre>
mount /dev/\<device\>3 /mnt
mkdir /mnt/boot
mount /dev/\<device\>1 /mnt/boot
swapon /dev/\<device\>2</pre>
1. Install packages: `pacstrap /mnt base base-devel linux linux-firmware iwd networkmanager neovim tmux git stow`
1. Generate fstab file: `genfstab -U /mnt >> /mnt/etc/fstab`
1. `arch-chroot /mnt`
    1. Link timezone to localtime: `ln -sf /usr/share/zoneinfo/<Region>/<City> /etc/localtime`
    1. `hwclock --systohc`
    1. Set language: `echo LANG=en_US.UTF-8 > /etc/locale.conf` and uncomment `en_US.UTF-8` in `nvim /etc/locale.gen`. Then run `locale-gen`
    1. Set console keyboard layout: `echo 'KEYMAP=dvorak' > /etc/vconsole.conf`
    1. Set hostname: `echo myhostname > /etc/hostname`
    1. `nvim /etc/hosts`:<pre>
127.0.0.1   localhost
::1         localhost
127.0.1.1   myhostname</pre>
    1. `nvim /etc/resolv.conf`:<pre>
nameserver  8.8.8.8
nameserver  8.8.4.4</pre>
    1. `nvim /etc/iwd/main.conf`:<pre>
[General]
EnableNetworkConfiguration=True</pre>
    1. Set root password: `passwd`
    1. Set the bootloader up (systemd-boot)
        1. `bootctl install`
        1. Create files (once with and once without "-fallback") `nvim /boot/loader/entries/arch-uefi(-fallback).conf`:<pre>
title   Arch Linux (Fallback)
linux   /vmlinuz-linux
initrd  /initramfs-linux(-fallback).img
options root=LABEL=ROOT rw</pre>
        1. `nvim /boot/loader/loader.conf`:<pre>
default arch-uefi.conf
timeout 3</pre>
        1. `bootctl update`
1. Exit chroot, unmount the partitions and reboot.


## Setting the system up

1. Connect wifi: `sytemctl enable --now iwd` and use `iwctl`
1. `useradd -mg users <USERNAME>` + `passwd <USERNAME>`
1. `EDITOR=nvim visudo` then uncomment `"%wheel ALL=(ALL) ALL"`
1. `gpasswd -a <USERNAME> wheel`
1. logout & login as <USERNAME>
1. Create folder structure: `mkdir -p Pictures/wallpaper Documents/gitstuff Documents/trash`
1. `git clone https://gitlab.com/WuerfelDev/dotfiles ~/.dotfiles`
1. `cd ~/.dotfiles`:
    1. `./link-dotfiles.sh`
    1. Install yay:<pre>
git clone https://aur.archlinux.org/yay.git ~/.cache/yay/yay
cd ~/.cache/yay/yay
makepkg -si</pre>
    1. Install console packages: `yay -S $(cat arch-packages-terminal.txt)`
    1. Install GUI packages `yay -S $(cat arch-packages-gui.txt)`
1. Change shell to fish: `chsh -s $(which fish)`


## Furter software and configuration

See [configuration.md](./configuration.md)
