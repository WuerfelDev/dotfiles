# dotfiles

> This replaces my old [i3 dotfiles repo](https://gitlab.com/WuerfelDev/dotfiles_wdev/).

**This script does not work as it is. It's only here for inspiration**

## Fresh Installation

1. Grab the latest Pop!OS iso from [here](https://pop.system76.com/)
2. Run `watch lsblk` then plug in your usb drive to see its name. Exit with *Ctrl+C*
3. (Unmount mounted partitions with `sudo umount /dev/sd<?><?>`)
4. Make sure the download has finished and continue to flash Pop!OS with `sudo dd bs=4M if=filename.iso of=/dev/sd<?> conv=fdatasync status=progress`. Be careful!
5. Boot the usb drive and follow the installer.
6. Boot the system and finish the installation.
7. Open Terminal (Super+T) and run `curl -L https://wuerfeldev.de/dot?fresh | bash` \
or manually run `git clone https://gitlab.com/WuerfelDev/dotfiles ~/.dotfiles` and `. ~/.dotfiles/installer.sh`
8. Reboot


## Lazy Installation

You can use one line to clone/install the dotfiles repo using the [server install helper script](https://gitlab.com/WuerfelDev/dotfiles/-/snippets/2106722). Add the same parameter to the website that you want the script to run with. No parameter (as below) will only clone the repo and does not run the installer.

**Attention! Only run it if you know what you are doing**

```
curl -L https://wuerfeldev.de/dot | bash
```


## Parameters

```
Installer
----------------------------------------------------
-a --applications        Install applications
-b --browser-extensions  Install browser extensions
-d --dotfiles            Install all dotfiles
-s --shell-experience    Gnome shell extensions
-f --fresh  --all        All above
-u --update              Pull the latest dotfiles
-h --help                This
```


## Gnome Shell Extensions

- [Blur My Shell](https://github.com/aunetx/blur-my-shell)
- [Caffeine](https://github.com/eonpatapon/gnome-shell-extension-caffeine)
- [Click to close overview](https://github.com/l3nn4rt/click-to-close-overview)
- [Clipboard indicator](https://github.com/Tudmotu/gnome-shell-extension-clipboard-indicator)
- [Colored AppMenu Icon](https://gitlab.com/yanbab/gnome-shell-extension-regular-appmenu-icon)
- [Improved Workspace Indicator](https://github.com/MichaelAquilina/improved-workspace-indicator)
- [LAN IP Address](https://github.com/Josholith/gnome-extension-lan-ip-address)
- [Multi Monitors](https://github.com/spin83/multi-monitors-add-on)
- [Simpler Off Menu](https://gitlab.com/K3rcus/simpler-off-menu)
- [Smart transparent topbar](https://github.com/aunetx/smart-transparent-top-bar)
- [Theme Switcher](https://gitlab.com/rmnvgr/nightthemeswitcher-gnome-shell-extension)
- [Transparent Shell](https://github.com/Siroj42/gnome-extension-transparent-shell)
- [Volume Mixer](https://github.com/aleho/gnome-shell-volume-mixer)
- [Workspace Scroll](https://github.com/squgeim/Workspace-Scroll)
- [Workspace Switch Wraparound](https://github.com/theychx/WorkspaceSwitcherWrapAround)


## Applications and Packages

- Apostrophe [f]
- Brave Browser
- GIMP [f]
- GParted
- KeePassXC [f]
- Telegram [f]
- (and remove libreoffice,geary,gnome-contacts)


For the terminal experience:
- alacritty + JetBrains Mono font
- fish
- starship
- nvim
- tmux
- at
- lolcat
- lynx
- mlocate
- neofetch
- sl
- tldr


f: via flatpak

## Browser extensions

The installation script requires the Brave Browser. A different (Chromium based) browser needs manual adjustment.

- [Gnome Shell Integration](https://gitlab.gnome.org/GNOME/chrome-gnome-shell)
- [I-don't-care-about-cookies](https://www.i-dont-care-about-cookies.eu/)
- [Readio Speed Reader](https://github.com/nicklassandell/Speed-reader)
- [Speed Control](https://github.com/milesjos/speed-control)
- [Vimium](https://github.com/philc/vimium)
