#!/usr/bin/env bash

usage()
{
    echo "Installer"
    echo "----------------------------------------------------"
    echo "-a --applications        Install applications"
    echo "-b --browser-extensions  Install browser extensions"
    echo "-d --dotfiles            Install all dotfiles"
    echo "-s --shell-experience    Gnome shell extensions"
    echo "-f --fresh  --all        All above"
    echo "-u --update              Pull the latest dotfiles"
    echo "-h --help                This"
}


#yes_or_no()
#{
#    while true; do
#        read -p "$* [y/n]: " yn
#        case $yn in
#            [Yy]*) return 0  ;;
#            [Nn]*) echo -e "\033[31mSkipped.\033[m" ; return  1 ;;
#        esac
#    done
#}

check_install()
{
    # https://stackoverflow.com/questions/1298066/how-to-check-if-package-installed-and-install-it-if-not#comment80142067_22592801
    if [[ $# -ne $(dpkg-query -W -f='${Status}' $@  | grep -c "ok installed") ]];
    then
        sudo apt install -y $@
    fi
}


PARSED_ARGS=$(getopt -n "$0" -o abdfhsu -l 'all,applications,browser-extensions,dotfiles,fresh,help,shell-experience,update' -- "$@")

# read getopt’s output this way to handle the quoting right:
eval set -- "$PARSED_ARGS"


applications=n
browser=n
dotfiles=n
shellexp=n


while true; do
    case "$1" in
        -a|--applications)
            applications=y
            shift
            ;;
        -b|--browser-extensions)
            browser=y
            shift
            ;;
        -d|--dotfiles)
            dotfiles=y
            shift
            ;;
        -f|--fresh|--all)
            applications=y
            browser=y
            dotfiles=y
            shellexp=y
            shift
            ;;
        -s|--shell-experience)
            shellexp=y
            shift
            ;;
        -h|--help)
            usage
            exit 0
            ;;
        -u|--update)
            cd ~/.dotfiles/
            git pull
            exit 0
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "unexpected argument"
            usage
            exit 1
            ;;
    esac
done

if [[ "$dotfiles" = n && "$applications" = n && "$shellexp" = n && "$browser" = n ]];
then
    usage
    exit 0
fi





# Linking dotfiles
if [[ "$dotfiles" = y ]];
then
    check_install stow
    cd ~/.dotfiles/dots/
    # Only stow if there are no complications
    until stow -nvt ~ *
    do
        echo "Manual action required!"
	read -n 1 -s -r -p "Press any key to try again"
    done
    stow -t ~ * 
    echo -e "\033[32m✓ Dotfiles linked\033[m"
    cd ~/.dotfiles/
fi




# Install applications and packages
if [[ "$applications" = y ]];
then
    # Temporary disable screen lock
    locksetting=$(gsettings get org.gnome.desktop.lockdown disable-lock-screen)
    lockdelay=$(gsettings get org.gnome.desktop.session idle-delay | awk -F' ' '{ printf $2 }')
    gsettings set org.gnome.desktop.lockdown disable-lock-screen true
    gsettings set org.gnome.desktop.session idle-delay 0

    # add Brave browser repository
    sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg
    echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main"|sudo tee /etc/apt/sources.list.d/brave-browser-release.list
    

    # Update+Install packages
    sudo apt update
    sudo apt upgrade -y --with-new-pkgs --allow-downgrades
    sudo apt install -y apt-transport-https curl tmux gnome-tweaks htop cowsay neofetch sl lolcat gparted xdotool python3-pip python-is-python3 libnotify-bin at tldr mlocate flatpak lynx brave-browser alacritty fish

    # Set default
    sudo chsh -s /usr/bin/fish
    sudo update-alternatives --set x-terminal-emulator /usr/bin/alacritty
    sudo update-alternatives --set x-www-browser /usr/bin/brave-browser-stable
    xdg-settings set default-web-browser brave-browser.desktop



    # Install starship prompt
    # https://github.com/starship/starship/discussions/1574#discussioncomment-49307
    curl -fsSL https://starship.rs/install.sh | bash -s -- --yes

    # Install JetBrains Mono font for terminal
    curl -fsSL https://raw.githubusercontent.com/JetBrains/JetBrainsMono/master/install_manual.sh | bash

    # Install Neovim nightly
    #https://github.com/neovim/neovim/wiki/Installing-Neovim#debian
    CUSTOM_NVIM_PATH=/usr/local/bin/nvim.appimage
    sudo curl -Lo $CUSTOM_NVIM_PATH https://github.com/neovim/neovim/releases/download/nightly/nvim.appimage
    sudo chmod a+x $CUSTOM_NVIM_PATH
    sudo update-alternatives --install /usr/bin/ex ex "${CUSTOM_NVIM_PATH}" 110
    sudo update-alternatives --install /usr/bin/vi vi "${CUSTOM_NVIM_PATH}" 110
    sudo update-alternatives --install /usr/bin/view view "${CUSTOM_NVIM_PATH}" 110
    sudo update-alternatives --install /usr/bin/vim vim "${CUSTOM_NVIM_PATH}" 110
    sudo update-alternatives --install /usr/bin/vimdiff vimdiff "${CUSTOM_NVIM_PATH}" 110

    # Clone tldr database
    tldr -u


    flatpak remote-add --user --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
    flatpak update -y
    
    flatpak install --noninteractive flathub org.telegram.desktop
    flatpak install --noninteractive flathub org.gimp.GIMP
    flatpak install --noninteractive flathub org.gnome.gitlab.somas.Apostrophe
    flatpak install --noninteractive flathub org.gnome.gitlab.somas.Apostrophe.Plugin.TexLive
    flatpak install --noninteractive flathub org.keepassxc.KeePassXC


    if [[ "$applications" = y ]];
    then
        sudo apt remove -y geary
        sudo apt purge -y libreoffice*
        sudo apt-get autoremove -y
        sudo apt-get clean
    fi


    gsettings set org.gnome.desktop.lockdown disable-lock-screen $locksetting
    gsettings set org.gnome.desktop.session idle-delay $lockdelay
    echo -e "\033[32m✓ Applications installed\033[m"
fi





# Install gnome extensions and set settings
if [[ "$shellexp" = y ]];
then
    # Temporary disable screen lock
    locksetting=$(gsettings get org.gnome.desktop.lockdown disable-lock-screen)
    lockdelay=$(gsettings get org.gnome.desktop.session idle-delay | awk -F' ' '{ printf $2 }')
    gsettings set org.gnome.desktop.lockdown disable-lock-screen true
    gsettings set org.gnome.desktop.session idle-delay 0


    ### Nautilus Terminal extension
    # https://github.com/flozz/nautilus-terminal
    check_install python3 python3-pip
    sudo apt install python3-nautilus -y
    pip3 install --user nautilus_terminal


    # Desktop setting changes
    #gsettings set org.gnome.desktop.wm.preferences button-layout 'appmenu:close'
    #gsettings set org.gnome.desktop.wm.preferences action-middle-click-titlebar 'minimize'
    #gsettings set org.gnome.desktop.interface enable-hot-corners true
    #gsettings set org.gnome.mutter edge-tiling false

    #gsettings set org.gnome.shell.extensions.dash-to-dock manualhide true
    gnome-extensions disable ding@rastersoft.com



    # Pop OS specific
    #gsettings set org.gnome.shell.extensions.pop-shell tile-by-default true
    #gsettings set org.gnome.shell.extensions.pop-shell smart-gaps true
    #gsettings set org.gnome.shell.extensions.pop-cosmic show-applications-button false
    #gsettings set org.gnome.shell.extensions.pop-cosmic show-workspaces-button false


    # Gnome terminal theming
    UUID=$(gsettings get org.gnome.Terminal.ProfilesList default | tr -d "'")
    gsettings set org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$UUID/ use-theme-transparency false
    gsettings set org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$UUID/ use-transparent-background true
    gsettings set org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:$UUID/ background-transparency-percent 20
    #gsettings set org.gnome.Terminal.Legacy.Settings default-show-menubar false

    # gedit preferences
    #gsettings set org.gnome.gedit.preferences.editor scheme 'pop-dark'
    #gsettings set org.gnome.gedit.preferences.editor insert-spaces true
    #gsettings set org.gnome.gedit.preferences.editor tabs-size 4
    #gsettings set org.gnome.gedit.preferences.editor wrap-mode 'word'
    #gsettings set org.gnome.gedit.preferences.editor wrap-last-split-mode 'word'

    # Install gnome extension installer
    # https://github.com/brunelli/gnome-shell-extension-installer
    wget -O gnome-shell-extension-installer "https://github.com/brunelli/gnome-shell-extension-installer/raw/master/gnome-shell-extension-installer"
    chmod +x gnome-shell-extension-installer
    sudo mv gnome-shell-extension-installer /usr/bin/

    # 3193 Blur My Shell
    # 3518 Transparent Shell
    # 3826 Click to close overview
    # 3712 Colored AppMenu Icon
    # 779  Clipboard indicator
    # 517  Caffeine
    # 2236 Theme Switcher
    # 1762 LAN IP Address
    # 3070 Simpler Off Menu
    # 858  Volume Mixer
    # 943  Workspace Scroll
    # 3968 Improved Workspace Indicator
    # 1116 Workspace Switch Wraparound
    # 2588 Smart transparent topbar
    gnome-shell-extension-installer 3193 3518 3826 3712 779 517 2236 1762 3070 858 943 3968 1116 2588

    
    # Blur My Shell
    #gsettings --schemadir ~/.local/share/gnome-shell/extensions/blur-my-shell@aunetx/schemas/ set org.gnome.shell.extensions.blur-my-shell sigma 30
    #gsettings --schemadir ~/.local/share/gnome-shell/extensions/blur-my-shell@aunetx/schemas/ set org.gnome.shell.extensions.blur-my-shell brightness .6
    # We dont use dock
    #gsettings --schemadir ~/.local/share/gnome-shell/extensions/blur-my-shell@aunetx/schemas/ set org.gnome.shell.extensions.blur-my-shell blur-dash false

    # Caffeine
    #gsettings --schemadir ~/.local/share/gnome-shell/extensions/caffeine@patapon.info/schemas set org.gnome.shell.extensions.caffeine show-notifications false

    # Theme Switcher
    #gsettings --schemadir ~/.local/share/gnome-shell/extensions/nightthemeswitcher@romainvigier.fr/schemas set org.gnome.shell.extensions.nightthemeswitcher.time always-enable-ondemand true

    # Simpler Off Menu
    #gsettings --schemadir ~/.local/share/gnome-shell/extensions/SimplerOffMenu.kerkus@pm.me/schemas set org.gnome.shell.extensions.simpleroffmenu show-settings false
    #gsettings --schemadir ~/.local/share/gnome-shell/extensions/SimplerOffMenu.kerkus@pm.me/schemas set org.gnome.shell.extensions.simpleroffmenu show-lock false
    #gsettings --schemadir ~/.local/share/gnome-shell/extensions/SimplerOffMenu.kerkus@pm.me/schemas set org.gnome.shell.extensions.simpleroffmenu show-suspend false

    # Improved Workspace Indicator
    #gsettings --schemadir ~/.local/share/gnome-shell/extensions/improved-workspace-indicator@michaelaquilina.github.io/schemas/ set org.gnome.shell.extensions.improved-workspace-indicator panel-position 'left'
    dconf load / < ~/.dotfiles/dconf

    nautilus -q

    # Restart GNOME Shell: https://stackoverflow.com/a/47952111/4346956
    busctl --user call org.gnome.Shell /org/gnome/Shell org.gnome.Shell Eval s 'Meta.restart("restarting gnome shell")'

    #FIX for https://github.com/brunelli/gnome-shell-extension-installer/issues/16
    sleep 10
    find ~/.local/share/gnome-shell/extensions/ -mindepth 1 -maxdepth 1 -type d -exec bash -c 'gnome-extensions enable $(basename "$0")' {} \;
    busctl --user call org.gnome.Shell /org/gnome/Shell org.gnome.Shell Eval s 'Meta.restart("restarting gnome shell")'



    gsettings set org.gnome.desktop.lockdown disable-lock-screen $locksetting
    gsettings set org.gnome.desktop.session idle-delay $lockdelay
    echo -e "\033[32m✓ Shell set up\033[m"
fi




# Install browser plugins
if [[ "$browser" = y ]];
then
    # Change this path for a different browser
    browserpath=/opt/brave.com/brave

    sudo mkdir -p $browserpath/extensions
    files=$(awk -F'[ #]' -v path="$browserpath/extensions/" '$1!="" { print path $1".json" }' ~/.dotfiles/browser-extensions)
    echo -e '{\n   "external_update_url": "https://clients2.google.com/service/update2/crx"\n}' | sudo tee $files 

    echo -e "\033[32m✓ Browser extensions installed\033[m"
fi




if [[ "$applications" = y || "$shellexp" = y ]];
then
    echo -e "\033[33mYou should reboot now. We're done :)\033[m"
fi
