#!/usr/bin/env bash

# Install browser extensions automatically
# default: brave
# Required packages: brave-bin

# Change this path for a different browser
#browserpath=/opt/brave.com/brave #brave in ubuntu
browserpath=/usr/lib/brave-bin/ #brave in arch 

sudo mkdir -p $browserpath/extensions
files=$(awk -F'[ #]' -v path="$browserpath/extensions/" '$1!="" { print path $1".json" }' ~/.dotfiles/browser-extensions)
echo -e '{\n   "external_update_url": "https://clients2.google.com/service/update2/crx"\n}' | sudo tee $files 

echo -e "\033[32m✓ Browser extensions installed\033[m"
